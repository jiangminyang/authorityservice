package com.sysomos.user.authority.sql;

import java.sql.PreparedStatement;
import java.util.List;
import java.util.Map;

public interface ISQLQuery<T> {

	PreparedStatement prepareStatement();

	T parseQueryResults(List<Map<String, SQLValue>> results);
}

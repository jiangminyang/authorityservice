package com.sysomos.user.authority.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.sysomos.user.authority.utils.ListUtils;
import com.sysomos.user.authority.utils.Pair;

public class LookupUserAuthorityScoreQuery extends BasicSQLStatement implements ISQLQuery<List<Pair<Long, Integer>>> {

	private static final String SQLQ = "select id, authority from user_scores where id in (%s)";

	public LookupUserAuthorityScoreQuery(List<Long> userIds) {
		super(String.format(SQLQ, ListUtils.join(userIds, ",")));
	}

	@Override
	public List<Pair<Long, Integer>> parseQueryResults(final List<Map<String, SQLValue>> results) {
		if (CollectionUtils.isEmpty(results))
			return null;

		List<Pair<Long, Integer>> allRows = new ArrayList<Pair<Long, Integer>>();
		
		System.out.println("results size: " + results.size());
		for (Map<String, SQLValue> row : results) {
			Pair<Long, Integer> pair = new Pair<Long, Integer>(row.get("id").getBigInteger().longValue(),
					row.get("authority").getInteger().intValue());
			allRows.add(pair);
		}
		return allRows;
	}

}
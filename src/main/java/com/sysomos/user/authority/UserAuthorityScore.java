package com.sysomos.user.authority;


public class UserAuthorityScore {

	private int authority;
	private final Long userId;

	public UserAuthorityScore(final Long userId, int authority) {
		this.userId = userId;
		this.authority = authority;
	}

	public Long getUserId() {
		return userId;
	}

	public int getAuthority() {
		return authority;
	}

	public String toString() {
		String toReturn = "User: " + userId + ", Authority: " + authority;
		return toReturn;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + authority;
		result = prime * result + userId.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserAuthorityScore other = (UserAuthorityScore) obj;
		if (authority != other.authority)
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}
	
	
}
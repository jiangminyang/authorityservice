package com.sysomos.user.authority;

import java.util.List;

/**
 * @author adia
 */
public class AuthorityScoreService {

	private static final int BATCH_SIZE = 300;

	/**
	 * This method returns an instance of {@link GetUserAuthorityScore.Request}. To
	 * get the authority of the user given in input, use this instance to call
	 * {@link com.sysomos.user.authority.GetUserAuthorityScore.Request#call()}
	 * . The authority is wrapped into a {@link ResultContext}. Below is an
	 * example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code Map<Long, Integer>} authority = resultContext.get(0);
	 * </code>
	 * </pre>
	 * 
	 * @param userId
	 * @return GetUserAuthorityScore.Request
	 */
	public static GetUserAuthorityScore.Request newGetUserAuthorityScoreRequest(long userId) {
		return new GetUserAuthorityScore.Request(userId);
	}

	/**
	 * This method returns an instance of {@link GetUserAuthorityScore.Request}. To
	 * get the authority of the user given in input, use this instance to call
	 * {@link com.sysomos.user.authority.GetUserAuthorityScore.Request#call()}
	 * . The authority is wrapped into a {@link ResultContext}. Below is an
	 * example of how to fetch the activity.
	 * 
	 * <pre>
	 * <code>
	 * {@code Map<Long, Integer>} authority = resultContext.get(0);
	 * </code>
	 * </pre>
	 * 
	 * @param List<long> userIds
	 * @return GetUserAuthorityScore.Request
	 */
	public static GetUserAuthorityScore.Request newGetUserAuthorityScoreRequest(List<Long> userIds) {
		return new GetUserAuthorityScore.Request(userIds);
	}

	/**
	 * This method returns an instance of {@link GetUserAuthorityScore.Batch}. This
	 * method should be called only when you want have a collection of userIds
	 * and want to fetch their authorities. Below is an example of how to fetch
	 * the activity.
	 * 
	 * <pre>
	 * <code>
	 * GetUserAuthorityScore.Batch batch = AuthorityScoreService.GetUserAuthorityScore();
	 * </code>
	 * Then you can either pass the batch a list of userIds or use inside a loop as per below
	 * <code>
	 * for (long id :  userIds) {
	 *    batch.addRequest(id);
	 * }
	 * </code>
	 * </pre>
	 * 
	 * @return GetUserAuthorityScore.Batch
	 */
	public static GetUserAuthorityScore.Batch newGetUserAuthorityScoreBatch() {
		return new GetUserAuthorityScore.Batch(BATCH_SIZE);
	}

}
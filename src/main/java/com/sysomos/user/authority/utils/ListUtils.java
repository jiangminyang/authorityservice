package com.sysomos.user.authority.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;import org.apache.commons.collections4.CollectionUtils;

public class ListUtils {

	public static List<Long> newList(long userId1, long userId2) {
		List<Long> ids = new ArrayList<Long>();
		ids.add(userId1);
		ids.add(userId2);
		return ids;
	}

	public static String join(Iterable<?> l, String glue) {
		if (l == null)
			return null;
		StringBuffer sb = new StringBuffer();
		boolean first = true;
		for (Object o : l) {
			if (!first) {
				sb.append(glue);
			}
			sb.append(o.toString());
			first = false;
		}
		return sb.toString();
	}
	
	public static String quotedJoin(Iterable<?> l, String glue){
		return "'" + join(l, "'" + glue + " '") + "'";
	}
	
	public static <T> List<T> dedup(Collection<T> l){
		Set <T> set = new HashSet<T>();
		
		set.addAll(l);
		
		return new ArrayList<T>(set);
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] toArray(List<T> list) {
		if (CollectionUtils.isEmpty(list))
			return null;
		Object[] ret = new Object[list.size()];
		list.toArray(ret);
		return (T[]) ret;
	}
}

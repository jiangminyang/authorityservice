package com.sysomos.user.authority.utils;


import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MapUtils {

	public static enum SortingOrder {
		ASC, DESC
	}

	/**
	 * @param map
	 *            a map
	 * @return return a map sorted in descending order
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(
			final Map<K, V> map, final SortingOrder order) {
		List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>(
				map.entrySet());
		Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
			public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
				if (SortingOrder.DESC == order) {
					return (o2.getValue()).compareTo(o1.getValue());
				} else {
					return (o1.getValue()).compareTo(o2.getValue());
				}
			}
		});

		Map<K, V> result = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}
}

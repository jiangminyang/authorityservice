package com.sysomos.user.authority.exception;

public class AuthorityScoreServiceException extends Exception {

	private static final long serialVersionUID = -6611428769319094677L;

	public AuthorityScoreServiceException(String message) {
		super(message);
	}

	public AuthorityScoreServiceException(Throwable cause) {
		super(cause);
	}

	public AuthorityScoreServiceException(Exception e) {
		super(e);
	}
}

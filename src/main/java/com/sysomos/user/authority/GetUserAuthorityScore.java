package com.sysomos.user.authority;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.Lists;
import com.sysomos.user.authority.exception.AuthorityScoreServiceException;
import com.sysomos.user.authority.sql.DatabaseUtils;
import com.sysomos.user.authority.sql.LookupUserAuthorityScoreQuery;
import com.sysomos.user.authority.utils.Pair;

public class GetUserAuthorityScore {

	private static final Logger LOG = LoggerFactory.getLogger(GetUserAuthorityScore.class);

	public static class Request {

		private final List<Long> userIds;

		Request(long userId) {
			userIds = new ArrayList<Long>();
			userIds.add(userId);
		}

		public Request(List<Long> userIds) {
			this.userIds = userIds;
		}

		public ResultContext call() throws AuthorityScoreServiceException {
			List<Request> requests = new ArrayList<Request>();
			requests.add(this);
			ResultContext rsltContext = new ResultContext();
			rsltContext.add(executeQueries(requests));
			return rsltContext;
		}
	}

	public static class Batch {

		private int batchSize;
		private final AtomicInteger countInBatch;
		private ConcurrentLinkedQueue<Request> queue;

		public Batch(int batchSize) {
			this.batchSize = batchSize;
			this.countInBatch = new AtomicInteger(0);
			queue = new ConcurrentLinkedQueue<Request>();
		}

		public void addRequest(long userId) {
			queue.add(new Request(userId));
		}

		/**
		 * Create a new {@link Request} based on the parameters given as inputs.
		 * 
		 * @param userIds
		 *            List of user IDs
		 */
		public void addRequest(final List<Long> userIds) {
			if (CollectionUtils.isEmpty(userIds))
				return;
			for (long id : userIds) {
				addRequest(id);
			}
		}

		private List<Pair<Long, Integer>> batchProcess() {
			int size, old;
			List<Pair<Long, Integer>> results = new ArrayList<Pair<Long, Integer>>();
			while (!queue.isEmpty()) {
				if (queue.size() < batchSize) {
					List<Request> requests = createBatch(queue.size());
					results.addAll(executeQueries(requests));
					break;
				}
				do {
					old = countInBatch.get();
					size = (old + 1) % batchSize;
				} while (!countInBatch.compareAndSet(old, size));
				if (size == 0) {
					List<Request> requests = createBatch(batchSize);
					results.addAll(executeQueries(requests));
					if (!queue.isEmpty()) {
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							LOG.error(e.getLocalizedMessage(), e);
						}
					}
				}
			}
			return results;
		}

		public ResultContext call() {
			ResultContext rslCtx = new ResultContext();
			rslCtx.add(batchProcess());
			return rslCtx;
		}

		private List<Request> createBatch(int size) {
			List<Request> batch = Lists.newArrayListWithCapacity(size);
			for (int i = 0; i < size; i++) {
				Request entry = queue.remove();
				if (entry == null) {
					LOG.warn("Queue has fewer elements than expected: " + batch.size() + " instead of " + size);
					break;
				}
				batch.add(entry);
			}
			return batch;
		}
	}

	private static List<Pair<Long, Integer>> executeQueries(final List<Request> requests) {
		if (CollectionUtils.isEmpty(requests))
			return null;
		List<Pair<Long, Integer>> lookupRslts;
		lookupRslts = new ArrayList<Pair<Long, Integer>>();
		LookupUserAuthorityScoreQuery query = null;
		DatabaseUtils dbUtils = new DatabaseUtils();
		for (Request req : requests) {
			query = new LookupUserAuthorityScoreQuery(req.userIds);
			lookupRslts.addAll(dbUtils.executeReadOnlySQLQuery(query));
		}
		return lookupRslts;
	}

	public static class ResultContext {

		private Map<Long, Integer> results;

		public ResultContext() {
			results = new HashMap<Long, Integer>();
		}

		public void add(final List<Pair<Long, Integer>> rows) {
			for (Pair<Long, Integer> keys : rows) {
				results.put(keys.getA(), keys.getB());
			}
		}

		public void add(final Pair<Long, Integer> result) {
			results.put(result.getA(), result.getB());
		}

		public Integer get(Long id) throws AuthorityScoreServiceException {
			if (results.get(id) == null)
				throw new AuthorityScoreServiceException("Invalid id");
			return results.get(id);
		}

		public int size() {
			return results.size();
		}
	}
}
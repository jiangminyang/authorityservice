package com.sysomos.user.authority.authorityService;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import com.sysomos.user.authority.AuthorityScoreService;
import com.sysomos.user.authority.GetUserAuthorityScore.ResultContext;
import com.sysomos.user.authority.exception.AuthorityScoreServiceException;

public class AuthorityScoreServiceTest {

	private static final List<Long> IDs = new ArrayList<Long>();

	static {
		IDs.add(223186092L);
		IDs.add(89848070L);
		IDs.add(2901854536L);
		IDs.add(2485244719L);
		IDs.add(1287002088L);
		IDs.add(2981817760L);
		IDs.add(1115981L);
		IDs.add(14123L);
		IDs.add(94452403L);
		IDs.add(18035030L);
		IDs.add(14435818L);
		IDs.add(362822566L);
		IDs.add(248541486L);
		IDs.add(21414060L);
		IDs.add(14133L);
		IDs.add(243630272L);
		IDs.add(14435836L);
		IDs.add(21414072L);
		IDs.add(868763869L);
		IDs.add(96965958L);
		IDs.add(12L);
		IDs.add(13L);
	}

	@Rule
	public TestRule watcher = new TestWatcher() {
		@Override
		protected void starting(Description description) {
			System.out.println("Starting test: " + description.getMethodName());
		}
	};

	@Before
	public void setUp() {

	}

	@Test
	public void newGetUserAuthorityScoreRequestTest() {
		try {
			ResultContext resultCtx = AuthorityScoreService.newGetUserAuthorityScoreRequest(IDs).call();
			assertTrue(resultCtx != null);
			assertTrue(resultCtx.size() > 0);
			
			for (int i = 10; i < 22; i++) {
				System.out.println(IDs.get(i) + " : " + resultCtx.get(IDs.get(i)));
			}
		} catch (AuthorityScoreServiceException e) {

		}

	}
}
